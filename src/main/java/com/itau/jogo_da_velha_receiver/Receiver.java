package com.itau.jogo_da_velha_receiver;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener(queues = "fila_da_mae")
public class Receiver {

	@RabbitHandler
	public void receive(String mensagem) {
		System.out.println(" [x] Received '" + mensagem + "'");
	}
} 

