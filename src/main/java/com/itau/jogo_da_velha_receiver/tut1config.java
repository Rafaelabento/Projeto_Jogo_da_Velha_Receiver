package com.itau.jogo_da_velha_receiver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.amqp.core.Queue;


@Profile({"tut1","fila_da_mae"})
@Configuration
public class tut1config {


	@Bean
	public Queue fila_da_mae() {
		return new Queue("fila_da_mae");
	}

	@Profile("receiver")
	@Bean
	public Receiver receiver() {
		return new Receiver();
	}

}

